# modules-lists-katina

A list component having a heading and a list either unordered or ordered. The content is defined in a .json file, its id can be passed using the options array.