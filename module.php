<?php

use Lewp\Resolve;
use Lewp\Config;

return new class extends Lewp\Module
{

    const KEY_ORDERED_LIST = "is_ordered";
    const KEY_HEADING = "heading";
    const KEY_HEADING_ELEMENT = "heading_element";
    const KEY_LIST = "list";
    const KEY_LIST_ITEM_TEXT = "text";
    const KEY_JSON_FILE = "json_file";

    const DEFAULT_IS_ORDERED = true;
    const DEFAULT_HEADING_ELEMENT = "div";

    private function loadData(string $filename)
    {
        return $this->loadJsonFile(Resolve::arrayToId([$this->getPageId(), $filename]));
    }

    private function createHeading(string $heading_element, string $heading)
    {
        return $this->createAndSetupElement(
            $heading_element,
            $heading,
            [
                "class" => "heading"
            ]
        );
    }

    private function createList(
        array $list_items,
        bool $is_ordered = self::DEFAULT_IS_ORDERED
    ) {
        if ($is_ordered) {
            $list = $this->createElement("ol");
        } else {
            $list = $this->createElement("ul");
        }
        $list->setAttribute("class", "list");

        $this->appendListItems($list, $list_items);
        return $list;
    }

    private function appendListItems(&$list, array $list_items)
    {
        foreach ($list_items as $item) {
            $text = ($item[self::KEY_LIST_ITEM_TEXT])
                ? $item[self::KEY_LIST_ITEM_TEXT]
                : '';
            unset($item[self::KEY_LIST_ITEM_TEXT]);
            $a = $this->createAndSetupElement(
                'a',
                $text,
                $item
            );

            $li = $this->createAndSetupElement(
                'li',
                '',
                [
                    "class" => "list-item"
                ]
            );
            $li->appendChild($a);
            $list->appendChild($li);
        }
    }

    public function run(array $options = []) : bool
    {
        $filename = $options[self::KEY_JSON_FILE];
        if (isset($filename)) {
            $options = array_merge($options, $this->loadData($options[self::KEY_JSON_FILE]));
        }
        if (!isset($options)) {
            trigger_error(
                'Module: '.$this->getModuleId().": ".
                'The specified JSON file "'.$filename.'"seems to be invalid. Please check it!',
                E_USER_WARNING
            );
        }
        // add default configuration
        $options += [
            self::KEY_ORDERED_LIST => self::DEFAULT_IS_ORDERED,
            self::KEY_HEADING_ELEMENT => self::DEFAULT_HEADING_ELEMENT
        ];
        // add heading if defined
        if (isset($options[self::KEY_HEADING])) {
            $this->appendChild(
                $this->createHeading($options[self::KEY_HEADING_ELEMENT],
                $options[self::KEY_HEADING])
            );
        }
        // add list
        if (!empty($options[self::KEY_LIST])) {
            $this->appendChild(
                $this->createList($options[self::KEY_LIST], $options[self::KEY_ORDERED_LIST])
            );
        }
        return true;
    }
};
